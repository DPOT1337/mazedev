package game2016;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javafx.application.Platform;

public class ClientThread
    extends Thread {

    private Main mainGame;
    private Socket clientSocket;
    private DataOutputStream outToServer;
    private BufferedReader inFromServer;
    private String IP = "localhost";
    private int port = 6789;

    private List<Player> players;
    
    public ClientThread(Main mainGame, Socket clientSocket, DataOutputStream outToServer,
        BufferedReader inFromServer) {
        super();
        this.mainGame = mainGame;
        this.clientSocket = clientSocket;
        this.outToServer = outToServer;
        this.inFromServer = inFromServer;
        this.players = new ArrayList<>();
    }
    
    @Override
    public void run() {
        
        boolean stopConnection = false;
//        List<Player> players = new ArrayList<>();

        System.out.println("Initializing ClientThread");
        
        try {
            while (!stopConnection) {
                String serverInput = inFromServer.readLine();
                players = new ArrayList<>();

//                System.out.println("FRA SERVER: " + serverInput + "|");
                String[] inputStr = serverInput.split("%");
//
//                for (String s : inputStr) {
//                    System.out.println(s);
//                }
//
//                System.out.println("inputStr length: " + inputStr.length);

                for (String go : inputStr) {
                    String[] playerObject = go.split("#");
//                    System.out.println("|");
//                    for (String s : playerObject) {
//                        System.out.println(s);
//                    }
//                    System.out.println("|");
                    String name = playerObject[0];
                    int x = Integer.parseInt(playerObject[1]);
                    int y = Integer.parseInt(playerObject[2]);
                    int point = Integer.parseInt(playerObject[3]);
                    String direction = playerObject[4];
                    
                    Player p = new Player(name, x, y, direction);
                    p.addPoints(point);
                    players.add(p);
                }

                Platform.runLater(new Runnable() {
                    @Override
                    public void run() {
                        mainGame.drawChanges(players);

                    }

                });
            }
        }
        catch (IOException e) {
            e.printStackTrace();
        }
        
    }
    
}
