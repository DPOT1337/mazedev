package game2016;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Socket;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.GridPane;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.stage.Stage;

//TODO : Hold styr på den gamle position (for ALLE spillere).
//TODO : En klient tråd der lytter efter updates fra serveren og displayer dem
/**
 * This is the client. It's merely a graphical representation and the client sends the
 * user actions to the server which then handles all the game logic and sends the updated
 * game state back to the clients so their graphical representations are up to date.
 *
 * @author cyberdynesystems
 *
 */
public class Main extends Application {

    private static Image image_floor;
    private static Image image_wall;
    private static Image hero_right, hero_left, hero_up, hero_down;

    //TODO Hvis vi får andre maps skal nogle tal her også blive sendt fra serveren.
    public static final int size = 20;
    public static final int scene_height = size * 20 + 100;
    public static final int scene_width = size * 20 + 200;

    private Label[][] fields;
    private TextArea scoreList;

    //-----------------------------------------------

    private Socket clientSocket;
    private DataOutputStream outToServer;
    private BufferedReader inFromServer;
    private String IP = "localhost";
    private int port = 6789;
    
    private ClientThread clientThread;
    
    private Map<String, int[]> playerPos;

    //-----------------------------------------------

    // -------------------------------------------
    // | Maze: (0,0)              | Score: (1,0) |
    // |-----------------------------------------|
    // | boardGrid (0,1)          | scorelist    |
    // |                          | (1,1)        |
    // -------------------------------------------

    @Override
    public void start(Stage primaryStage) {

        playerPos = new HashMap<>();

        try {

            GridPane grid = new GridPane();
            grid.setHgap(10);
            grid.setVgap(10);
            grid.setPadding(new Insets(0, 10, 0, 10));

            Text mazeLabel = new Text("Maze:");
            mazeLabel.setFont(Font.font("Arial", FontWeight.BOLD, 20));

            Text scoreLabel = new Text("Score:");
            scoreLabel.setFont(Font.font("Arial", FontWeight.BOLD, 20));

            scoreList = new TextArea();

            GridPane boardGrid = new GridPane();

            initializeImages();

            //--------------------------------------------------
            //Sets up the connection to the server and draws the map.
            initializeConnection(boardGrid);
            
            //--------------------------------------------------
            
            scoreList.setEditable(false);

            grid.add(mazeLabel, 0, 0);
            grid.add(scoreLabel, 1, 0);
            grid.add(boardGrid, 0, 1);
            grid.add(scoreList, 1, 1);

            Scene scene = new Scene(grid, scene_width, scene_height);
            primaryStage.setScene(scene);
            primaryStage.show();

            scene.addEventFilter(KeyEvent.KEY_PRESSED, event -> {
                switch (event.getCode()) {
                case UP:
                    playerMoved(0, -1, "up");
                    break;
                case DOWN:
                    playerMoved(0, +1, "down");
                    break;
                case LEFT:
                    playerMoved(-1, 0, "left");
                    break;
                case RIGHT:
                    playerMoved(+1, 0, "right");
                    break;
                default:
                    break;
                }
            });

        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    //Initializes the pictures.
    private void initializeImages() {
        image_wall = new Image(getClass().getResourceAsStream("Image/wall4.png"), size, size,
            false, false);
        image_floor = new Image(getClass().getResourceAsStream("Image/floor1.png"), size, size,
            false, false);

        hero_right = new Image(getClass().getResourceAsStream("Image/heroRight.png"), size,
            size, false, false);
        hero_left = new Image(getClass().getResourceAsStream("Image/heroLeft.png"), size, size,
            false, false);
        hero_up = new Image(getClass().getResourceAsStream("Image/heroUp.png"), size, size,
            false, false);
        hero_down = new Image(getClass().getResourceAsStream("Image/heroDown.png"), size, size,
            false, false);
    }

    //Initialize the connection with the server and choose a player name.
    private void initializeConnection(GridPane boardGrid) throws Exception {
        BufferedReader inFromUser = new BufferedReader(new InputStreamReader(System.in));
        clientSocket = new Socket(IP, port);
        outToServer = new DataOutputStream(clientSocket.getOutputStream());
        inFromServer = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
        
        //Choose a name
        System.out.print("Choose a name:\n>");
        String sentence = inFromUser.readLine();
        outToServer.writeBytes(sentence + '\n');

        //Initialized the map
        initializeMap(boardGrid);

        //Start the thread listening for server inputs.
        System.out.println("BOARD INITIALIZED, STARTING CLIENT THREAD!");

        //Create a thread that listens for server input.
        clientThread = new ClientThread(this, clientSocket, outToServer, inFromServer);
        
        clientThread.start();
    }

    //Initializes the map sent from the server.
    private void initializeMap(GridPane boardGrid) throws IOException, Exception {
        //Get the map from the server.
        String m = inFromServer.readLine();
        String[] board = m.split("#");

        //represent it in the GUI
        fields = new Label[20][20];
        for (int j = 0; j < board.length; j++) {
            for (int i = 0; i < board[j].length(); i++) {
                switch (board[j].charAt(i)) {
                case 'w':
                    fields[i][j] = new Label("", new ImageView(image_wall));
                    break;
                case ' ':
                    fields[i][j] = new Label("", new ImageView(image_floor));
                    break;
                default:
                    throw new Exception("Illegal field value: " + board[j].charAt(i));
                }
                boardGrid.add(fields[i][j], i, j);
            }
        }
    }

    public void playerMoved(int delta_x, int delta_y, String direction) {
        //TODO Communicate with server the changes, and make the server handle all the game logic...
        //Server then resolves the move with all its implications
        //and sends the new game state to ALL the clients.
        String data = delta_x + "#" + delta_y + "#" + direction;
        try {
            outToServer.writeBytes(data + "\n");
        }
        catch (IOException e) {
            e.printStackTrace();
        }
        
    }

    public void drawChanges(List<Player> players) {
        StringBuffer b = new StringBuffer(100);
        for (Player p : players) {
            drawPlayer(p);
            b.append(p);
        }
        //Draw the scoreboard
        scoreList.setText(b.toString());
    }
    
    private void drawPlayer(Player p) {
        String direction = p.getDirection();
        String name = p.getName();
        int x = p.getXpos();
        int y = p.getYpos();

        int[] oldPos = playerPos.get(name);
        if (oldPos != null) {
            fields[oldPos[0]][oldPos[1]].setGraphic(new ImageView(image_floor));
        }
        
        playerPos.put(name, new int[] { x, y });
        
        if (direction.equals("right")) {
            fields[x][y].setGraphic(new ImageView(hero_right));
        }
        else if (direction.equals("left")) {
            fields[x][y].setGraphic(new ImageView(hero_left));
        }
        else if (direction.equals("up")) {
            fields[x][y].setGraphic(new ImageView(hero_up));
        }
        else {
            fields[x][y].setGraphic(new ImageView(hero_down));
        }
    }

    public static void main(String[] args) {
        launch(args);
    }
}