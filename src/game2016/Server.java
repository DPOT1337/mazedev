package game2016;

import java.io.DataOutputStream;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;

public class Server {

    //All connected clients.
    private static List<ServerThread> clientConns = new ArrayList<>();
    
    private static MazeGame maze;

    /**
     * @param args
     */
    public static void main(String[] args) throws Exception {
        //Initialize the maze game
        maze = new MazeGame();

        initMessage();
        
        ServerSocket welcomeSocket = new ServerSocket(6789);
        
        while (true) {
            Socket connectionSocket = welcomeSocket.accept();
            System.out.println("New client connected!");
            ServerThread client = new ServerThread(connectionSocket, maze);
            clientConns.add(client);
            client.start();
        }
    }

    public synchronized static void updateAllClients() throws IOException {
        //TODO send the new game state (all the player objects) to all the clients who is connected
        String a = "";
        for (int i = 0; i < maze.getPlayers().size(); i++) {
            a = a + maze.getPlayers().get(i).getName() + "#" + maze.getPlayers().get(i).getXpos()
                + "#" + maze.getPlayers().get(i).getYpos() + "#"
                + maze.getPlayers().get(i).getPoint() + "#"
                + maze.getPlayers().get(i).getDirection() + "%";
        }
        System.out.println("Sender: " + a);
        for (int i = 0; i < clientConns.size(); i++) {
            DataOutputStream outStream = clientConns.get(i).getDataOutputStream();
            outStream.writeBytes(a + "\n");
        }

    }

    private static void initMessage() {
        System.out.println("Initializing the server...");
    }

}
