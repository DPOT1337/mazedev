package game2016;

import java.net.*;

import java.io.*;

/**
 * ServerThread uses the static methods in Server class when for example
 * @author cyberdynesystems
 *
 */
public class ServerThread extends Thread {
    private Socket connSocket;
    private MazeGame maze;
    private Player thisPlayer; //TODO ved ikke om det er nødvendigt at holde styr på spilleren her.
    private DataOutputStream outToClient;
    
    public ServerThread(Socket connSocket, MazeGame maze) {
        this.connSocket = connSocket;
        this.maze = maze;
    }
    
    @Override
    public void run() {
        try {
            BufferedReader inFromClient =
                new BufferedReader(new InputStreamReader(connSocket.getInputStream()));
            outToClient = new DataOutputStream(connSocket.getOutputStream());
            
            String chosenName = "Unknown";
            
            //Set the player name to the chosen one:
            chosenName = inFromClient.readLine();
            
            //The server then creates the player and spawns him in a free location.
            initializePlayer(chosenName);
            
            //Send maze to the client.
            String map = "";
            String[] maze = this.maze.getBoard();
            for (String s : maze) {
                map += s + "#";
            }
            outToClient.writeBytes(map + "\n");
            
            //At last, update all clients with the new information. (ALL player positions and scores).
            Server.updateAllClients();
            
            //----------------------------------
            // Listen for client moves:
            
            boolean stopConnection = false;

            while (!stopConnection) {
                String move = inFromClient.readLine();
//                System.out.println("move: " + move);
                String[] pMove = move.split("#");
//                System.out.println("move after split: ");
////                for (String s : pMove) {
//                    System.out.println(s);
//                }
//                System.out.println("|");
                this.maze.playerMoved(thisPlayer, Integer.parseInt(pMove[0]),
                    Integer.parseInt(pMove[1]), pMove[2]);
                
                Server.updateAllClients();
            }
            
        }
        catch (IOException e) {
            e.printStackTrace();
        }
        // do the work here
    }
    
    private void initializePlayer(String name) {
        //Creates the player and spawns him on the server.
        thisPlayer = maze.createPlayer(name);
    }
    
    public Socket getConnSocket() {
        return connSocket;
    }
    
    public DataOutputStream getDataOutputStream() {
        return outToClient;
    }
}