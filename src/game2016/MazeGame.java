package game2016;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * This class contains the game logic.
 *
 * @author cyberdynesystems
 *
 */
public class MazeGame {

    private static List<Player> players = new ArrayList<>();
    
    private String[] board = {    // 20x20
        "wwwwwwwwwwwwwwwwwwww",
        "w        ww        w",
        "w w  w  www w  w  ww",
        "w w  w   ww w  w  ww",
        "w  w               w",
        "w w w w w w w  w  ww",
        "w w     www w  w  ww",
        "w w     w w w  w  ww",
        "w   w w  w  w  w   w",
        "w     w  w  w  w   w",
        "w ww ww        w  ww",
        "w  w w    w    w  ww",
        "w        ww w  w  ww",
        "w         w w  w  ww",
        "w        w     w  ww",
        "w  w              ww",
        "w  w www  w w  ww ww",
        "w w      ww w     ww",
        "w   w   ww  w      w",
        "wwwwwwwwwwwwwwwwwwww"
    };

    /**
     *
     * @param player
     * @param delta_x
     * @param delta_y
     * @param direction
     */
    public synchronized void playerMoved(Player player, int delta_x, int delta_y,
        String direction) {
        player.setDirection(direction);
        int x = player.getXpos(), y = player.getYpos();
        
        if (board[y + delta_y].charAt(x + delta_x) == 'w') {
            player.addPoints(-1);
        }
        else {
            Player p = getPlayerAt(x + delta_x, y + delta_y);
            if (p != null) {
                player.addPoints(10);
                p.addPoints(-10);
            }
            else {
                player.addPoints(1);
                
//                fields[x][y].setGraphic(new ImageView(image_floor));
                
                x += delta_x;
                y += delta_y;
//
                player.setXpos(x);
                player.setYpos(y);
            }
        }
//        scoreList.setText(getScoreList());
    }

    public Player getPlayerAt(int x, int y) {
        for (Player p : players) {
            if (p.getXpos() == x && p.getYpos() == y) {
                return p;
            }
        }
        return null;
    }
    
    public String getScoreList() {
        StringBuffer b = new StringBuffer(100);
        for (Player p : players) {
            b.append(p + "\r\n");
        }
        return b.toString();
    }

    /**
     * Creates a player and spawns the character on a free location.
     *
     * @param name
     * @return
     */
    public Player createPlayer(String name) {
        //Find a free position on the map.
        int[] xyLoc = findFreeSpawn();
        String direction = "up"; //TODO Choose a random direction.

        Player p = new Player(name, xyLoc[0], xyLoc[1], direction);
        
        players.add(p);
        
        return p;
    }
    
    public List<Player> getPlayers() {
        return new ArrayList<>(players);
    }
    
    public String[] getBoard() {
        return board;
    }
    
    public void setBoard(String[] board) {
        this.board = board;
    }
    
    //Returns an array with [ x, y ] locations.
    private int[] findFreeSpawn() {
        int[] xyLoc = new int[2];
        int x = 0;
        int y = 0;
        boolean found = false;
        
        //Keep looping until a legit position has been found.
        while (!found) {
            y = (int) Math.floor(Math.random() * 18) + 1;
            x = (int) Math.floor(Math.random() * 18) + 1;
            
            int i = 0;
            if (board[y].charAt(x) != 'w') {
                found = true;
                while (found && i < players.size()) {
                    Player p = players.get(i);
                    found = p.getXpos() != x && p.getYpos() != y;
                    i++;
                }
            }
        }
        xyLoc[0] = x;
        xyLoc[1] = y;
        return xyLoc;
    }
}
